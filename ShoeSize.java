import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/* ShoeSize - Eric McCreath 2015 - GPL 
 * This class stores a persons shoe size.
 */

public class ShoeSize {
	private static final String SHOESIZEENAME = "SHOESIZE";
	public static final int SHOESIZEMAX = 15;
	public static final int SHOESIZEMIN = 3;

	static final String FILENAME = "shoesize.xml";

	private Integer shoesize;

	public ShoeSize() {
		shoesize = null;
	}

	public String show() {
		return (shoesize == null ? "" : shoesize.toString());
	}

	public boolean set(Integer v) {
		if (v == null || v >= ShoeSize.SHOESIZEMIN && v <= ShoeSize.SHOESIZEMAX) {
			shoesize = v;
			save();
			return true;
		} else {
			shoesize = null;
			return false;
		}
	}

	static ShoeSize load() {
		// add code here that will load shoe size from a file called "FILENAME"
		ShoeSize ans = new ShoeSize();
		
		File f = new File(FILENAME);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		
		try {
			// load the xml tree
			db = dbf.newDocumentBuilder();
			Document doc = db.parse(f);
			
			Node shoe = doc.getFirstChild();
			Node n = shoe.getChildNodes().item(0);

			ans.shoesize = Integer.parseInt(n.getTextContent());

		} catch (Exception e) {
			System.err.println("Problem loading " + FILENAME);
		}
	
		return ans;
	}

	void save() {
		// add code here that will save shoe size into a file called "FILENAME"
		File f = new File(FILENAME);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			// make the xml tree
			db = dbf.newDocumentBuilder();
			Document doc = db.newDocument();
			Element shoe = doc.createElement("shoe");
			
			Element es = doc.createElement("size");
			es.appendChild(doc.createTextNode(Integer.toString(shoesize)));
			shoe.appendChild(es);
			
			doc.appendChild(shoe);

			// save the xml file
			TransformerFactory transformerFactory = TransformerFactory
					.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			
			// set xml encoding to utf-8
			transformer.setOutputProperty(OutputKeys.ENCODING,"utf-8");

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(f);
			transformer.transform(source, result);
		} catch (Exception e) {
			System.err.println("Problem saving " + FILENAME);
		}

	}
}
